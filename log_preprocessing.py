# -*- coding: utf-8 -*-
"""
Created on Thu Nov  5 12:40:49 2020

@author: tosic
"""

import numpy as np # for math
import matplotlib.pyplot as plt #visualising data
import pandas as pd # for managing datasers


#Import ds and specify folder where it is
dataset = pd.read_csv('Logs.csv')

#Analasys of specific columns:
    
hosts = dataset["Host"].value_counts()
dates = dataset["Date"].value_counts()
files = dataset["Page/File"].value_counts()
url_param = dataset["URL parameter"].value_counts()
bndwt = dataset["Bandwidth"].value_counts()
codes = dataset["Response Code"].value_counts()
method = dataset["Request Method"].value_counts()
referrers = dataset["Referrers"].value_counts()
user_agents = dataset["User Agent"].value_counts()

#EMPTY are:
    
users = dataset["User"].value_counts()
v_domain = dataset["Virtual Domain"].value_counts()
cookies = dataset["Cookie"].value_counts()

#example of getting one get one column
#col_codes = dataset.loc[:,"Response Code"]

#dropping emty cols

dataset.drop('User',axis='columns', inplace=True)
dataset.drop('Virtual Domain',axis='columns', inplace=True)
dataset.drop('Cookie',axis='columns', inplace=True)
dataset.drop('URL parameter',axis='columns', inplace=True)

#droping rows if colum User Agent is empty

dataset = dataset.dropna(axis=0, subset=['User Agent'])

#Cleaning ResponseCodes (!200) and extensions from Requests

dataset = dataset[dataset["Response Code"]=='200 - OK']
searchfor = ['.png', '.jpg','.gif','.css','.jpeg','.cgi','.bot','.mp3','.js','.ico','.swf','.tif','.bmp']
dataset = dataset[~dataset["Page/File"].str.contains('|'.join(searchfor))]
dataset = dataset[~dataset["Referrers"].str.contains('|'.join(searchfor))]

#Cleaning logs with no Referrer and no Page/File data

dataset = dataset.drop(dataset[(dataset['Referrers'] == 'No Referrer') & (dataset['Page/File'] == '/')].index)

#Cleaning Robot requests

search_robottxt = ['robots.txt']
search_agents = ['Googlebot',' AdsBot','Bingbot','BingPreview','facebookexternalhit','Slurp','spider','sogou','proximic','Baiduspider','DuckDuckBot','Yandexbot','ia_archiver','crawl','kraw']

dataset = dataset[~dataset["Page/File"].str.contains('|'.join(search_robottxt))]
dataset = dataset[~dataset["User Agent"].str.contains('|'.join(search_agents))]

dataset = dataset.drop(dataset[(dataset['Referrers'] == 'No Referrer') & (dataset['Request Method'] == 'HEAD')].index)

#Filling null values in Bandwidth column with most frequent value
#TODO: Check if that should be done after splitting data set to training and test!
dataset['Bandwidth'].fillna((dataset['Bandwidth'].mode()), inplace=True)

#changing datatypes of specific columns
dataset["Date"] = pd.to_datetime(dataset["Date"])
#droping ALL empty values NOW
dataset = dataset.dropna()
dataset = dataset.sort_values(by = 'Date',ascending = True) 
dataset.to_csv(path_or_buf = "Preprocessed_Logs.csv",columns = ["Host","Date",
                                                                    "Page/File",
                                                                    "Bandwidth",
                                                                    "Response Code",
                                                                    "Request Method",
                                                                    "Referrers",
                                                                    "User Agent"])
#result
length = len(dataset)
info = dataset.info()
describe = dataset.describe()