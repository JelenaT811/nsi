# -*- coding: utf-8 -*-
"""
Created on Mon Nov 23 16:44:59 2020

@author: korisnik
"""
import datetime
import operator
import functools
import csv
import pandas as pd
from itertools import islice
import uuid 

dataset = pd.read_csv('Preprocessed_Logs.csv')
ipDict = {}

recordsRead = 0
progressThreshold = 100
sessionTimeout = datetime.timedelta(minutes=30)
column_names = ["Host","User Agent","Request Time","Session Number","URL"]
df = pd.DataFrame(columns=column_names,index = range(120000))
ID = 0
for i in dataset.index:
    if (i == 0):
        continue
    else:
        recordsRead += 1
        #print(dataset.loc)
        # print "line of %d records: %s\n" % (len(fields), line)
        if (recordsRead >= progressThreshold):
            print("Read %d records" % recordsRead)
            progressThreshold *= 2
    
        # http://www.dblab.ntua.gr/persdl2007/papers/72.pdf
        #   "a new request is put in an existing session if two conditions are valid:
        #    * the IP address and the user-agent are the same of the requests already
        #      inserted in the session,
        #    * the request is done less than fifteen minutes after the last request inserted."
    
        newRequestTime = pd.to_datetime(dataset["Date"][i])
        host,agent,page,ref = dataset["Host"][i], dataset["User Agent"][i],dataset["Page/File"][i],dataset["Referrers"][i]
        url = [[page,ref]]
        if host not in  ipDict:
            ID = ID + 1
            ipDict[host] = {agent: {1:  [newRequestTime, url, ID]}}
        else:
            if agent not in ipDict[host]:
                ID = ID + 1
                ipDict[host] = {agent: {1: [newRequestTime, url, ID]}}
            else:#NOVA SESIJA
                ID = ID+1
                keys = list(ipDict[host][agent].keys())
                session_num = keys[0]
                #print(session_num)
                reqTime =  pd.to_datetime(ipDict[host][agent][session_num][0])
                s_sum = session_num
                if newRequestTime - reqTime >= sessionTimeout: #Nova sesija
                    ID = ID + 1
                    s_sum = session_num+1
                    #print(s_sum)
                    ipDict[host] = {agent: {s_sum: [newRequestTime, url, ID]}}
                    #print(ipDict[host][agent])
                    #print(ipDict[host][agent])
                    #print(list(ipDict.keys())[0])
                else:#Nije nova stara je samo treba dodati pod frejm
                    #ne treba url od i !!!! nego od host+ userangent+sesija
                    ipDict[host][agent][session_num][0] = newRequestTime
                    ipDict[host][agent][session_num][2] = ID
                    url1 = [page,ref]
                    ipDict[host][agent][session_num][1].append(url1)
    
                  
column_names = ["Host","User Agent","Session ID","Request Time","URL", "ID"]
df = pd.DataFrame(columns=column_names,index = range(120000))
print(recordsRead)

#Extract to df

i=0
url_dict = {}
for key in ipDict:
    df["Host"][i]= key
    for user_key in ipDict[key]:
         df["User Agent"][i]= user_key
         for ses_id in ipDict[key][user_key]:
              df["Session ID"][i]= ses_id  
              df["Request Time"][i]= ipDict[key][user_key][ses_id][0] 
              df["URL"][i]= ipDict[key][user_key][ses_id][1] 
              ID = ipDict[key][user_key][ses_id][2]
              df["ID"][i]= ID 
              list = ipDict[key][user_key][ses_id][1]
              for pair in list:
                  if len(pair)==2:
                     # print(pair)
                      url_dict[pair[0]] = { pair[1]: ID}
    i=i+1

df.to_csv(path_or_buf = "Sessions_Extracted_New.csv")


#Extract urls to dataframe:

iter = 0;
col_names = ["Page/File","Referrers", "ID"]
dataframe_url = pd.DataFrame(columns=col_names,index = range(18644))
for key in url_dict:
     dataframe_url["Page/File"][iter]= key
     for ref_key in url_dict[key]:
         dataframe_url["Referrers"][iter]= ref_key
         dataframe_url["ID"][iter] = url_dict[key][ref_key]
     iter = iter +1
dataframe_url.to_csv(path_or_buf = "Urls_Extracted.csv")
#End of extraction           
